# Experis Chinook + DbSuperheroes

# Chinook C# Project
## Introduction
This project is an assignment for the .Net course at Noroff. The project gives a couple functionalities. 9 different functions to get data from the database.

## Requirements
- Microsoft Visual Studio 2019 or higher
- .Net 5.0 or higher
- SQL Server 2017 or higher

## Installation
Clone the repository: git clone https://gitlab.com/noroffassignments/assignment-2-chinook.git
Import the chinook.sql file to your SQL Server
Open the solution file in Visual Studio
Update the connection string in the ConnectionHelper.cs file with your database credentials
Build and run the solution

## Usage
The application allows users to perform the following actions:
- Read all customers
- Read specific customers by ID
- Read specific customers by Name
- Return a page of customers by offset and amount
- Create new Customers
- Update existing customers
- Return the number of customers in each country
- Return the customers by spending high to low
- Return specific customer by ID with most popular genre

## Contributing
- Niels van Mierlo https://gitlab.com/NVM238
- Niek Veenstra https://gitlab.com/NiekVeenstra

## License
This project is licensed under the MIT License - see the LICENSE file for details.

# DbSuperheroes SQL scripts
## Introduction
This project is an assignment for the .Net course at Noroff. The project gives 9 script to return specific functions.

## Requirements
- SSMS 19
- SQL Server 2017 or higher

## Installation
Clone the repository: git clone https://gitlab.com/noroffassignments/assignment-2-chinook.git
Run the scripts in the SQL_database folder on your SQL Server

## Usage
The scripts will run the following:
- Create a Database
- Create tables
- Create relationship between superhero and assistant
- Create relationship between superhero and power
- Create superheroes
- Create assistants
- Create powers
- Update a superhero
- Delete an assistant

## Contributing
- Niels van Mierlo https://gitlab.com/NVM238
- Niek Veenstra https://gitlab.com/NiekVeenstra

## License
This project is licensed under the MIT License - see the LICENSE file for details.

