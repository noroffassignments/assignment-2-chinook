﻿using System;

namespace Chinook_new.Menus
{
    public enum MenuType
    {
        MainMenu,
        AllCustomersMenu,
        CustomerbyIDMenu,
        CustomersByNameMenu,
        AddNewCustomerMenu,
        UpdateCustomerMenu,
        CustomerCountryMenu,
        CustomerSpenderMenu,
        CustomerGenreByIdMenu,
    }
    public class MainMenu : MenuBase
    {
        private readonly MenuType[] _menuTypes =
        {
            MenuType.AllCustomersMenu,
            MenuType.CustomerbyIDMenu,
            MenuType.CustomersByNameMenu,
            MenuType.AddNewCustomerMenu,
            MenuType.UpdateCustomerMenu,
            MenuType.CustomerCountryMenu,
            MenuType.CustomerSpenderMenu,
            MenuType.CustomerGenreByIdMenu
        };

        public override void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Show all customers\n2. Show customer by ID\n3. Show customers by Name\n4. Add new customer\n5. Update existing customer\n6. List countries based on customer count\n7. List customers based on spending\n8. Customers most popular genre\n\nClose program: Q");

            // get the key pressed by the user
            var key = Console.ReadKey(true).Key;
            // check if the user pressed Q to close the program
            if (key == ConsoleKey.Q)
            {
                ReturnToParentMenu();
                return;
            }

            // check if the user pressed a number between 1 and 8
            if (key >= ConsoleKey.D1 && key <= ConsoleKey.D8)
            {
                int index = key - ConsoleKey.D1;
                Console.WriteLine($"{_menuTypes[index]} selected.");
                // call the SetMenu method with the selected option
                MenuManager.SetMenu(_menuTypes[index]);
            }
        }

        // method to close the program
        public static void ReturnToParentMenu()
        {
            Environment.Exit(0);
        }
    }
}
