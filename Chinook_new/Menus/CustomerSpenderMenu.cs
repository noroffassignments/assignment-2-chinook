﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chinook_new.Menus
{
    // Class that represents the menu for displaying customers by highest spenders
    internal class CustomerSpenderMenu : MenuBase
    {
        // Display the menu for displaying customers by highest spenders
        public override void ShowMenu()
        {
            // Create an instance of the CustomerSpenderRepository
            ICustomerSpenderRepository repository = new CustomerSpenderRepository();
            
            // Calls the PrintByHighestSpender method and passes in the repository data
            PrintByHighestSpender(repository);
        }

        // Method that takes in a repository and calls the GetHighestCustomerSpenders method and prints the result
        private static void PrintByHighestSpender(ICustomerSpenderRepository repository)
        {
            Console.Clear();

            // Calls PrintCustomersByHighestSpender method and passes in the result of calling the GetHighestCustomerSpenders method
            PrintCustomersByHighestSpender(repository.GetHighestCustomerSpenders());

            Console.WriteLine($"Esc. Go back");

            // Makes the user go bakc to the main menu when selected esc
            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }

        }

        // Method that takes in a list of CustomerSpender objects and prints them to the console
        static void PrintCustomersByHighestSpender(IEnumerable<CustomerSpender> customerSpenders)
        
        {
            var enumerable = customerSpenders as CustomerSpender[] ?? customerSpenders.ToArray();
            if (enumerable.Any())
            {
                foreach (CustomerSpender customerSpender in customerSpenders)
                {
                    Console.WriteLine($"ID: {customerSpender.CustomerId}, Name: {customerSpender.FirstName} {customerSpender.LastName}, Total spent: ${customerSpender.TotalSpendMoney}");
                }
            }
        }

        // Method that returns to the main menu
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }

    }
}
