﻿using System;

namespace Chinook_new.Menus
{
    public static class MenuManager
    {
        private static MenuType CurrentMenu = MenuType.MainMenu;
        private static readonly MainMenu mainMenu = new MainMenu();
        private static readonly AllCustomersMenu allCustomersMenu = new AllCustomersMenu();
        private static readonly CustomerbyIDMenu customerbyIDMenu = new CustomerbyIDMenu();
        private static readonly CustomersByNameMenu customersByNameMenu = new CustomersByNameMenu();
        private static readonly AddNewCustomerMenu addNewCustomerMenu = new AddNewCustomerMenu();
        private static readonly UpdateCustomerMenu updateCustomerMenu = new UpdateCustomerMenu();
        private static readonly CustomerCountryMenu customerCountryMenu = new CustomerCountryMenu();
        private static readonly CustomerSpenderMenu customerSpenderMenu = new CustomerSpenderMenu();
        private static readonly CustomerGenreByIdMenu customerGenreByIdMenu = new CustomerGenreByIdMenu();

        public static void ShowMenus()
        {
            Console.Clear();
            // Show the current menu based on the value of CurrentMenu
            while (true)
            {
                switch (CurrentMenu)
                {
                    case MenuType.MainMenu:
                        mainMenu.ShowMenu();
                        break;
                    case MenuType.AllCustomersMenu:
                        allCustomersMenu.ShowMenu();
                        break;
                    case MenuType.CustomerbyIDMenu:
                        customerbyIDMenu.ShowMenu();
                        break;
                    case MenuType.CustomersByNameMenu:
                        customersByNameMenu.ShowMenu();
                        break;
                    case MenuType.AddNewCustomerMenu:
                        addNewCustomerMenu.ShowMenu();
                        break;
                    case MenuType.UpdateCustomerMenu:
                        updateCustomerMenu.ShowMenu();
                        break;
                    case MenuType.CustomerCountryMenu:
                        customerCountryMenu.ShowMenu();
                        break;
                    case MenuType.CustomerSpenderMenu:
                        customerSpenderMenu.ShowMenu();
                        break;
                    case MenuType.CustomerGenreByIdMenu:
                        customerGenreByIdMenu.ShowMenu();
                        break;
                }
            }
        }

        public static void SetMenu(MenuType newMenu)
        {
            CurrentMenu = newMenu;
        }
    }
}