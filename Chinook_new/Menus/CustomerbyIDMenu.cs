﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Menus
{
    internal class CustomerbyIDMenu : MenuBase
    {
        public override void ShowMenu()
        {
            //Create an instance of the ICustomerRepository
            ICustomerRepository repository = new CustomerRepository();
            //call the PrintOneRecord method passing the repository as parameter
            PrintOneRecord(repository);
        }

        //Method to print only one record of a customer by ID
        static void PrintOneRecord(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Type the ID of the customer you would like to find.");

            //Store the user input in the userInput variable
            string userInput = Console.ReadLine();

            //Input for the testing
            //call the PrintCustomer method passing the repository.GetCustomer(userInput) as parameter
            PrintCustomer(repository.GetCustomer(userInput));

            Console.WriteLine($"Esc. Go back");

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        //Method to print only one customer
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.CustomerId}, {customer.FirstName}, {customer.LastName}, {customer.Country}, {customer.PostalCode}, {customer.Phone}, {customer.Email}");
        }

        //Method to return to MainMenu
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}
