﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;

namespace Chinook_new.Menus
{
    internal class AllCustomersMenu : MenuBase
    {
        // ShowMenu function creates an instance of the ICustomerRepository interface and calls the PrintAllRecords function
        public override void ShowMenu()
        {
            ICustomerRepository repository = new CustomerRepository();
            PrintAllRecords(repository);
        }

        // PrintAllRecords function prompts the user for the number of customers to display and the starting point of the display
        // it then calls the PrintCustomers function and displays an escape message to return to the previous menu
        private static void PrintAllRecords(ICustomerRepository repository)
        {
            Console.Clear();
            Console.Write("How many customers would you like to display: ");
            int userInputMax = Int32.Parse(Console.ReadLine());

            Console.Clear();
            Console.Write("At what customer would you like to start by number: ");
            int userInputOffset = Int32.Parse(Console.ReadLine());

            PrintCustomers(repository.GetAllCustomers(userInputMax, userInputOffset));

            Console.WriteLine($"Esc. Go back");

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }

        }

        // PrintCustomers function takes in an IEnumerable of customers and iterates through the list, calling the 
        // PrintCustomer function for each customer in the list
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        // PrintCustomer function takes in a customer object and prints the customer's properties in a formatted string
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.CustomerId}, {customer.FirstName}, {customer.LastName}, {customer.Country}, {customer.PostalCode}, {customer.Phone}, {customer.Email}");
        }

        // ReturnToParentMenu function sets the current menu to the MainMenu
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}
