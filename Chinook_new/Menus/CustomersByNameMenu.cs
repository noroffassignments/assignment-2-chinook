﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;

namespace Chinook_new.Menus
{
    internal class CustomersByNameMenu : MenuBase
    {
        public override void ShowMenu()
        {
            ICustomerRepository repository = new CustomerRepository();
            PrintAllRecords(repository);
        }

        static void PrintAllRecords(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Type the name of the customer you would like to find.");

            string userInput = Console.ReadLine();

            //Input for the testing
            PrintCustomers(repository.GetCustomersByName(userInput));

            Console.WriteLine($"Esc. Go back");

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        //for printing all customers
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer); //Invoke this method and print cw statement
            }
        }

        static void PrintCustomer(Customer customer)
        // To print only 1 customer
        {
            Console.WriteLine($"{customer.CustomerId}, {customer.FirstName}, {customer.LastName}, {customer.Country}, {customer.PostalCode}, {customer.Phone}, {customer.Email}");
        }

        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}
