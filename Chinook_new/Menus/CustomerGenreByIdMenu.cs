﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Menus
{
    internal class CustomerGenreByIdMenu : MenuBase
    {
        public override void ShowMenu()
        {
            // Create an instance of the CustomerGenreRepository
            ICustomerGenreRepository repository = new CustomerGenreRepository();

            // Calls the PrintPopularGenreCustomer method and passes in the repository data
            PrintPopularGenreCustomer(repository);
        }

        // Method that takes in a repository and calls the GetCustomerPopularGenreById method and prints the result
        private static void PrintPopularGenreCustomer(ICustomerGenreRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Type the ID of the customer for their most popular genre.");

            string userInput = Console.ReadLine();

            // Calls PrintCustomerMostPopularGenre method and passes in the result of calling the GetCustomerPopularGenreById method
            PrintCustomerMostPopularGenre(repository.GetCustomerPopularGenreById(userInput));

            Console.WriteLine($"Esc. Go back");

            // Makes the user go bakc to the main menu when selected esc
            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }

        }

        private static void PrintCustomerMostPopularGenre(IEnumerable<CustomerGenre> customerGenres)
        {
            var enumerable = customerGenres as CustomerGenre[] ?? customerGenres.ToArray();
            if (enumerable.Any())
            {
                foreach (CustomerGenre customerGenre in customerGenres)
                {
                    Console.WriteLine($"ID: {customerGenre.CustomerId}, Name: {customerGenre.FirstName} {customerGenre.LastName}, Most popular genre: {customerGenre.Genre} {customerGenre.GenreCount}");
                }
            }
        }

        // Method that returns to the main menu
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}
