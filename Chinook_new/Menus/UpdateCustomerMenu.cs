﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;

namespace Chinook_new.Menus
{
    internal class UpdateCustomerMenu : MenuBase
    {
        public override void ShowMenu()
        {
            // Create a new instance of the ICustomerRepository interface
            ICustomerRepository repository = new CustomerRepository();
            UpdateRecord(repository);
        }

        static void UpdateRecord(ICustomerRepository repository)
        {
            Console.Clear();
            Console.Write("Customer ID: ");
            // Get the customer ID entered by the user
            string customerId = Console.ReadLine();
            // Get the customer object from the repository using the entered ID
            Customer customer = repository.GetCustomer(customerId);

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change First name to: ");
            customer.FirstName = Console.ReadLine();

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change Last name to: ");
            customer.LastName = Console.ReadLine();

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change Country to: ");
            customer.Country = Console.ReadLine();

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change Postal code to: ");
            customer.PostalCode = Console.ReadLine();

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change Phone number to: ");
            customer.Phone = Console.ReadLine();

            Console.Clear();
            PrintCustomer(customer);
            Console.Write("Change Email address to: ");
            customer.Email = Console.ReadLine();

            // Update value in db
            if (repository.UpdateCustomer(customer))
            {
                // Print the updated customer information
                Console.Clear();
                Console.WriteLine("Successfully Updated record");
                PrintCustomer(customer);
            }
            else
            {
                Console.WriteLine("Failed to update record");
            }

            Console.WriteLine($"Esc. Go back");

            // Prompt the user to press Esc. key to return to the previous menu
            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        static void PrintCustomer(Customer customer)
        {
            // Print the customer's information in a formatted string
            Console.WriteLine($"{customer.CustomerId}, {customer.FirstName}, {customer.LastName}, {customer.Country}, {customer.PostalCode}, {customer.Phone}, {customer.Email}");
        }

        public static void ReturnToParentMenu()
        {
            // Set the current menu to the MainMenu
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}
