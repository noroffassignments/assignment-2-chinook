﻿using Chinook_new.Models;
using System.Collections.Generic;
using System;
using System.Text;

namespace Chinook_new.Menus
{
    public abstract class MenuBase
    {
        public abstract void ShowMenu();
    }
}