﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Chinook_new.Menus
{
    internal class CustomerCountryMenu : MenuBase
    {
        // Overrides the ShowMenu method from the base class to display the customers by country.
        public override void ShowMenu()
        {
            // Create an instance of the customer country repository
            ICustomerCountryRepository repository = new CustomerCountryRepository();
            PrintByCountry(repository);
        }

        // Method to print the customers by country
        private static void PrintByCountry(ICustomerCountryRepository repository)
        {
            Console.Clear();

            // Call the GetCustomersByCountry method from the repository
            PrintCustomersByCountry(repository.GetCustomersByCountry());

            Console.WriteLine($"Esc. Go back");

            // Wait for user to press the escape key to return to the main menu
            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }

        }

        // Method to print the customers by country
        static void PrintCustomersByCountry(IEnumerable<CustomerCountry> customerCountries)
        {
            var enumerable = customerCountries as CustomerCountry[] ?? customerCountries.ToArray();
            if (enumerable.Any())
            {
                // Iterate through the list of customer countries and print the country and count of customers
                foreach (CustomerCountry customerCountry in customerCountries)
                {
                    Console.WriteLine($"Country: {customerCountry.Country}, Amount of customers: {customerCountry.Count}");
                }
            }
        }

        // Method to return to the main menu
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }

    }
}
