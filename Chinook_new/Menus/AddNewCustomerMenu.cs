﻿using Chinook_new.Models;
using Chinook_new.Repository;
using System;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace Chinook_new.Menus
{
    internal class AddNewCustomerMenu : MenuBase
    {
        // ShowMenu is the main method of the class, it is called when the user selects the corresponding menu option.
        // It creates an instance of the CustomerRepository class and calls the InsertRecord method.
        public override void ShowMenu()
        {
            ICustomerRepository repository = new CustomerRepository();
            InsertRecord(repository);
        }

        // InsertRecord method takes in an ICustomerRepository object.
        // It prompts the user to input customer information and creates a new Customer object with the inputted information.
        // It then calls the repository's AddNewCustomer method, passing in the new customer object.
        // If the method returns true, it prints "Successfully inserted record" to the console.
        // If the method returns false, it prints "Failed to inserted record" to the console.
        // It also provides an escape option to return to the main menu.
        static void InsertRecord(ICustomerRepository repository)
        {
            Console.Clear();
            Console.Write("First name: ");
            string firstName = Console.ReadLine();

            Console.Clear();
            Console.Write("Last name: ");
            string lastName = Console.ReadLine();

            Console.Clear();
            Console.Write("Country: ");
            string country = Console.ReadLine();

            Console.Clear();
            Console.Write("Postal code: ");
            string postalCode = Console.ReadLine();

            Console.Clear();
            Console.Write("Phone number: ");
            string phone = Console.ReadLine();

            Console.Clear();
            Console.Write("Emailaddress: ");
            string email = Console.ReadLine();

            Customer newCustomer = new Customer()
            {
                FirstName = firstName,
                LastName = lastName,
                Country = country,
                PostalCode = postalCode,
                Phone= phone,
                Email= email,
            };

            if (repository.AddNewCustomer(newCustomer))
            {
                Console.Clear();
                Console.WriteLine("Successfully inserted record");
            } else
            {
                Console.WriteLine("Failed to inserted record");
            }

            Console.WriteLine($"Esc. Go back");

            bool selected = false;
            while (!selected)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Escape:
                        ReturnToParentMenu();
                        selected = true;
                        break;
                }
            }
        }

        // ReturnToParentMenu method sets the current menu to the main menu.
        public static void ReturnToParentMenu()
        {
            MenuManager.SetMenu(MenuType.MainMenu);
        }
    }
}