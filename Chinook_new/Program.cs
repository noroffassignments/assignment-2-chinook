﻿using Chinook_new.Models;
using Chinook_new.Repository;
using Chinook_new.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MenuManager.ShowMenus();
        }
    }
}
