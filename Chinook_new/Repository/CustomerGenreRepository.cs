﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;
using System.Windows.Forms;

namespace Chinook_new.Repository
{
    internal class CustomerGenreRepository : ICustomerGenreRepository
    {
        public List<CustomerGenre> GetCustomerPopularGenreById(string id)
        {
            // List to store the customer most popular genre
            List<CustomerGenre> customerGenreList = new List<CustomerGenre>();

            // SQL statement to select the customer most popular genre
            string sql = $@"WITH Data AS( SELECT
                            CustomerData.CustomerId as CustId, CustomerData.FirstName as CustFirstName, CustomerData.LastName as CustLastName, CustomerData.Genre as CustGenre, CustomerData.GenreCount as CustCount
                         FROM(
                            SELECT Invoice.CustomerId as CustomerId, Customer.FirstName as FirstName, Customer.LastName as LastName, Genre.Name as Genre, COUNT(*) as GenreCount
                         FROM Invoice
                            JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId
                            JOIN Track ON InvoiceLine.TrackId = Track.TrackId
                            JOIN Genre ON Track.GenreId = Genre.GenreId
                            JOIN Customer ON Invoice.CustomerId = Customer.CustomerId
                         WHERE Customer.CustomerId = {id} GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name ) AS CustomerData )
                            SELECT CustId, CustFirstName, CustLastName, CustGenre, CustCount FROM Data WHERE CustCount = (SELECT MAX(CustCount) FROM Data)";

            try
            {
                // Connect to the database and open the connection
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Create a new SqlCommand with the SQL statement and the connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Execute the SQL statement and get the result set
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read the result set and add the customer most popular genre to the list
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Genre = reader.GetString(3);
                                temp.GenreCount = reader.GetInt32(4).ToString();
                                customerGenreList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customerGenreList;
        }
    }
}
