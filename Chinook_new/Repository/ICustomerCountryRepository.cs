﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Repository
{
    internal interface ICustomerCountryRepository
    {
        List<CustomerCountry> GetCustomersByCountry(); //To get all the customers by country
    }
}
