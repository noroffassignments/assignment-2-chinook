﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Repository
{
    public interface ICustomerRepository
    {
        List<Customer> GetAllCustomers(int maxCount = 1000, int offset = 0); //To get all the customers
        Customer GetCustomer(string id); //To get a single customer
        List<Customer> GetCustomersByName(string firstName); //To get all the customers with some name

        bool AddNewCustomer(Customer customer); //To add a new customer to db
        bool UpdateCustomer(Customer customer); //To update a customer in the db
    }
}
