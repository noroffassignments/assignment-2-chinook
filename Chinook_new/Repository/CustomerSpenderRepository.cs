﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;

namespace Chinook_new.Repository
{
    internal class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        // Get the highest spending customers
        public List<CustomerSpender> GetHighestCustomerSpenders()
        {
            // List to store the customer spenders
            List<CustomerSpender> customerSpenderList = new List<CustomerSpender>();

            // SQL statement to select the customer spenders
            string sql = @"WITH TotalSpending AS (
                         SELECT CustomerId, SUM(Total) AS TotalSpent FROM Invoice GROUP BY CustomerId)
                         SELECT Customer.CustomerId, FirstName, LastName, TotalSpent
                         FROM Customer
                         JOIN TotalSpending
                         ON Customer.CustomerId = TotalSpending.CustomerId
                         ORDER BY TotalSpent DESC";

            try
            {
                // Connect to the database and open the connection
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Create a new SqlCommand with the SQL statement and the connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Execute the SQL statement and get the result set
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read the result set and add the customer spenders to the list
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.TotalSpendMoney = reader.GetValue(3).ToString();
                                customerSpenderList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customerSpenderList;
        }
    }
}
