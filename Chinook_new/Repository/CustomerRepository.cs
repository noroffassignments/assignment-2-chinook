﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Chinook_new.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        // Add new customer
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            // SQL query to insert a new customer into the Customer table
            string sql = "Insert into Customer(FirstName, LastName, Country, PostalCode, Phone, Email) Values(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                // create a new connection to the database using the ConnectionHelper
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // create a new command using the sql query and the open connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // add the customer's information as parameters to the query
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        //ExecuteNonQuery() returns the number of rows affected by the query
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }


        public List<Customer> GetAllCustomers(int maxCount = 1000, int offset = 0)
        {
            //To get all customer from the database
            List<Customer> customerList = new List<Customer>();
            //Select statement
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {maxCount} ROWS ONLY";

            try
            {
                // create a new connection to the database using the ConnectionHelper
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                //Assign values from database to the below fields
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                //Add to the collection List.
                                customerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;

        }

        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();

            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where CustomerId=@ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0).ToString();
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;

        }

        public List<Customer> GetCustomersByName(string firstName)
        {
            List<Customer> customerList = new List<Customer>();

            string sql = $"select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where FirstName LIKE '%{firstName}%'";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;

        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "update Customer set FirstName=@FirstName, LastName=@LastName, Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email where CustomerId=@CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex) {
                Console.WriteLine(ex.Message);
            }
            return success;

        }
    }
}
