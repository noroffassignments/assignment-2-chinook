﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Repository
{
    internal interface ICustomerSpenderRepository
    {
        List<CustomerSpender> GetHighestCustomerSpenders(); //To get the highest spending customers
    }
}
