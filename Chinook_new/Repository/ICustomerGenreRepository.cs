﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Repository
{
    internal interface ICustomerGenreRepository
    {
        List<CustomerGenre> GetCustomerPopularGenreById(string id); //To get the most popular genre for customer
    }
}
