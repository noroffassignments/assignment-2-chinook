﻿using Chinook_new.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook_new.Repository
{
    internal class CustomerCountryRepository : ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomersByCountry()
        {
            // List to store the amount of customers per country
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();

            // SQL statement to select the countries and count
            string sql = $"SELECT Country, Count(*) as Count FROM Customer GROUP BY Country ORDER BY Count DESC";
            try
            {
                // Connect to the database and open the connection
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Create a new SqlCommand with the SQL statement and the connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Execute the SQL statement and get the result set
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Read the result set and add the countries and count to the list
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(0);
                                temp.Count = reader.GetInt32(1);
                                customerCountryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customerCountryList;
        }

    }
}
