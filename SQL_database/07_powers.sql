USE SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES ('Code Breaker', 'Easily remember or figure out complex codes.');

INSERT INTO Power (Name, Description)
VALUES ('Microscopic vision', 'See the tiniest details.');

INSERT INTO Power (Name, Description)
VALUES ('Shinobi magic', 'Use supernaturel ninja technic.');

INSERT INTO Power (Name, Description)
VALUES ('Alcoholism', 'Drink untill you fall in a coma.');

INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (1,1);
INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (1,2);
INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (1,3);

INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (2,3);
INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (2,4);

INSERT INTO HeroPowerConnection (SuperheroId, PowerId)
VALUES (3,1);