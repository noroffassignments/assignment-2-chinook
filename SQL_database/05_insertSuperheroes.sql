USE SuperheroesDb;

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Niek', 'Caramel Seasalt', 'You received them when a strange project involving a razor and a magical bear worked better than expected.');

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Niels', 'Bouncy Putty', 'You bought them from an alien from another dimension.');

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Saju', 'Aqua Sodium', 'You got them during an incident involving a computer game and a toothpick.');