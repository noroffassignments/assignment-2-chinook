USE SuperheroesDb;

CREATE TABLE HeroPowerConnection(
	SuperheroId int REFERENCES Superhero(ID),
	PowerId int REFERENCES Power(ID),
	PRIMARY KEY (SuperheroId, PowerId)
);